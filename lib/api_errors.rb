##
# Base `ApiErrors` module and configuration.
module ApiErrors
  class << self
    ##
    # Instructions, as a `Proc`, on how to log an error to the project's log
    # file.
    attr_accessor :log_callback

    ##
    # Instructions, as a `Proc`, on how to track an error to an error tracking
    # system.
    attr_accessor :track_callback

    ##
    # The default formatter to use for all errors. It can be overridden by
    # each errors if needed.
    attr_writer :formatter

    ##
    # Convenience method to use in the project's initializer file.
    #
    # ```ruby
    # ApiErrors.configure do |config|
    #   config.formatter = :json_api
    #   config.enable_i18n!
    # end
    # ```
    def configure
      yield self
    end

    ##
    # Enables internationalization, and allows `I18n` to translate error
    # attributes when appropriate. It can be overridden by each errors.
    def enable_i18n!
      @i18n_enabled = true
    end

    ##
    # Returns a boolean indicating whether the translation is currently enabled.
    def i18n_enabled?
      @i18n_enabled ||= false
    end

    ##
    # Returns the current formatter. Defaults to
    # `ApiErrors::Formatters::SimpleFormatter`.
    def formatter
      @formatter ||= Formatters::SimpleFormatter
    end

    ##
    # In order to build up an error registry, one or more YAML files need to be
    # created and loaded with this method. See the `README.md` file for
    # information about YAML files and registries.
    def register_errors_from_yaml(file_path)
      YAML
        .load_file(file_path)
        .each do |registry_code, args|
          register_error(
            registry_code.to_sym,
            args.deep_symbolize_keys
          )
        end
    end

    ##
    # Registers a single error with the specified code. The arguments passed are
    # all attributes, parameters and options in a single `Hash` object.
    #
    # ```ruby
    # ApiErrors.register_error(
    #   :b01247,
    #   status: 403
    # )
    # ```
    #
    # The `ApiErrors::ApiError` class will be determined by the status. If you
    # need to specify the error class manually, the `error` key can be given
    # the error class' name, as a string. This results in the same error as the
    # previous example:
    #
    # ```ruby
    # ApiErrors.register_error(
    #   :b01247,
    #   error: 'ApiErrors::ForbiddenError'
    # )
    # ```
    #
    # This method should rarely be used, in favor of the one which loads a file
    # with all error definitions, `register_errors_from_yaml`.
    def register_error(registry_code, args)
      error = args.delete(:error)
      status = args.delete(:status)

      ApiErrors::Register.add(
        registry_code,
        get_error_class(error, status),
        args.delete(:detail),
        args.delete(:code) || registry_code.to_s,
        args.delete(:title),
        status,
        args
      )
    end

    ##
    # Using the error registry, this method creates an instance of the error
    # type associated with the specified code.
    #
    # The error `detail` can be overridden if necessary, depending on the
    # current context in which the error is occuring. Some parameters and
    # options can also be specified to supplement the ones in the registry, such
    # as data only available at run-time when the error occurs.
    #
    # By design, the error types should never change their `status`, `code`, nor
    # `title` attributes - only `detail` reflects the current instance of the
    # error type.
    #
    # The `detail` is optionally specified as the second argument, while the
    # parameters and options are specified last.
    #
    #   raise ApiErrors.code(:a0001)
    #   raise ApiErrors.code(:a0002,
    #                        'User with id 350 failed.',
    #                        translate: false)
    def code(code, *args)
      options = args.last.is_a?(Hash) ? args.pop.dup : {}
      detail = args.shift

      ApiErrors::Register.build_error(code, detail, options)
    end

    private

    def get_error_class(error, status)
      return Object.const_get(error) if error

      api_errors_by_status[status] || ApiErrors::ApiError
    end

    def api_errors_by_status
      @descendants ||=
        ApiErrors
          .constants
          .map { |const| ApiErrors.const_get(const) }
          .select { |const| const < ApiErrors::ApiError }
          .reject { |const| const == ApiErrors::AggregatedError }
          .each_with_object({}) { |const, o| o[const.default_status] = const }
    end
  end
end

require 'i18n'
require 'i18n/core_ext/hash'
require 'yaml'

require 'api_errors/api_error'
require 'api_errors/aggregated_error'
require 'api_errors/client_errors/client_error'
require 'api_errors/server_errors/server_error'

require 'api_errors/register'

require 'api_errors/formatters/formatter'
require 'api_errors/formatters/json_api_formatter'
require 'api_errors/formatters/simple_formatter'
require 'api_errors/formatters/string_formatter'
