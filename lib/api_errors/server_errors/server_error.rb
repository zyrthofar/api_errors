module ApiErrors
  # Base error for 5XX errors
  class ServerError < ApiError; end # :nodoc:
end

require 'api_errors/server_errors/internal_server_error'
require 'api_errors/server_errors/not_implemented_error'
require 'api_errors/server_errors/bad_gateway_error'
require 'api_errors/server_errors/service_unavailable_error'
require 'api_errors/server_errors/gateway_timeout_error'
require 'api_errors/server_errors/http_version_not_supported_error'
require 'api_errors/server_errors/variant_also_negotiates_error'
require 'api_errors/server_errors/insufficient_storage_error'
require 'api_errors/server_errors/loop_detected_error'
require 'api_errors/server_errors/not_extended_error'
require 'api_errors/server_errors/network_authentication_required_error'
