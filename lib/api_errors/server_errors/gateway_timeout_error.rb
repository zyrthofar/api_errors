module ApiErrors
  # HTTP 504 - GATEWAY TIMEOUT
  class GatewayTimeoutError < ServerError # :nodoc:
    DEFAULT_STATUS = 504
    DEFAULT_CODE = 'gateway_timeout'.freeze
    DEFAULT_TITLE = 'Gateway Timeout'.freeze
    DEFAULT_DETAIL = 'Gateway timeout error.'.freeze
  end
end
