module ApiErrors
  # HTTP 505 - HTTP VERSION NOT SUPPORTED
  class HttpVersionNotSupportedError < ServerError # :nodoc:
    DEFAULT_STATUS = 505
    DEFAULT_CODE = 'http_version_not_supported'.freeze
    DEFAULT_TITLE = 'HTTP Version Not Supported'.freeze
    DEFAULT_DETAIL = 'HTTP version not supported error.'.freeze
  end
end
