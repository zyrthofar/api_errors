module ApiErrors
  # HTTP 510 - NOT EXTENDED
  class NotExtendedError < ServerError # :nodoc:
    DEFAULT_STATUS = 510
    DEFAULT_CODE = 'not_extended'.freeze
    DEFAULT_TITLE = 'Not Extended'.freeze
    DEFAULT_DETAIL = 'Not extended error.'.freeze
  end
end
