module ApiErrors
  # HTTP 503 - SERVICE UNAVAILABLE
  class ServiceUnavailableError < ServerError # :nodoc:
    DEFAULT_STATUS = 503
    DEFAULT_CODE = 'service_unavailable'.freeze
    DEFAULT_TITLE = 'Service Unavailable'.freeze
    DEFAULT_DETAIL = 'Service unavailable error.'.freeze
  end
end
