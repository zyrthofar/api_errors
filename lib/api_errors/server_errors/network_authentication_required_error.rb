module ApiErrors
  # HTTP 511 - NETWORK AUTHENTICATION REQUIRED
  class NetworkAuthenticationRequiredError < ServerError # :nodoc:
    DEFAULT_STATUS = 511
    DEFAULT_CODE = 'network_authentication_required'.freeze
    DEFAULT_TITLE = 'Network Authentication Required'.freeze
    DEFAULT_DETAIL = 'Network authentication required error.'.freeze
  end
end
