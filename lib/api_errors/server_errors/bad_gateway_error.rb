module ApiErrors
  # HTTP 502 - BAD GATEWAY
  class BadGatewayError < ServerError # :nodoc:
    DEFAULT_STATUS = 502
    DEFAULT_CODE = 'bad_gateway'.freeze
    DEFAULT_TITLE = 'Bad Gateway'.freeze
    DEFAULT_DETAIL = 'Bad gateway error.'.freeze
  end
end
