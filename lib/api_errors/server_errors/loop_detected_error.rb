module ApiErrors
  # HTTP 508 - LOOP DETECTED
  class LoopDetectedError < ApiError # :nodoc:
    DEFAULT_STATUS = 508
    DEFAULT_CODE = 'loop_detected'.freeze
    DEFAULT_TITLE = 'Loop Detected'.freeze
    DEFAULT_DETAIL = 'Loop detected error.'.freeze
  end
end
