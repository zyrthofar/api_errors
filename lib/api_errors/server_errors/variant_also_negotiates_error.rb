module ApiErrors
  # HTTP 506 - VARIANT ALSO NEGOTIATES
  class VariantAlsoNegotiatesError < ServerError # :nodoc:
    DEFAULT_STATUS = 506
    DEFAULT_CODE = 'variant_also_negotiates'.freeze
    DEFAULT_TITLE = 'Variant Also Negotiates'.freeze
    DEFAULT_DETAIL = 'Variant also negotiates error.'.freeze
  end
end
