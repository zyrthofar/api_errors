module ApiErrors
  # HTTP 507 - INSUFFICIENT STORAGE
  class InsufficientStorageError < ServerError # :nodoc:
    DEFAULT_STATUS = 507
    DEFAULT_CODE = 'insufficient_storage'.freeze
    DEFAULT_TITLE = 'Insufficient Storage'.freeze
    DEFAULT_DETAIL = 'Insufficient storage error.'.freeze
  end
end
