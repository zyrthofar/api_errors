module ApiErrors
  # HTTP 500 - INTERNAL SERVER ERROR
  class InternalServerError < ServerError # :nodoc:
    DEFAULT_STATUS = 500
    DEFAULT_CODE = 'internal_server_error'.freeze
    DEFAULT_TITLE = 'Internal Server Error'.freeze
    DEFAULT_DETAIL = 'Internal server error.'.freeze
  end
end
