module ApiErrors
  # HTTP 501 - NOT IMPLEMENTED
  class NotImplementedError < ServerError # :nodoc:
    DEFAULT_STATUS = 501
    DEFAULT_CODE = 'not_implemented'.freeze
    DEFAULT_TITLE = 'Not Implemented'.freeze
    DEFAULT_DETAIL = 'Not implemented error.'.freeze
  end
end
