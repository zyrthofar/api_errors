module ApiErrors
  module Formatters
    ##
    # `ApiErrors::Formatters::JsonApiFormatter` formats an `ApiErrors::ApiError`
    # to a `Hash` object following the JSON-API standard specification related
    # to error objects.
    #
    # ref. [JSON-API errors](http://jsonapi.org/format/#errors)
    #
    # ref. [JSON-API error objects](http://jsonapi.org/examples/#error-objects)
    #
    # ## Aggregated Errors
    #
    # The JSON-API formatter accepts `ApiErrors::AggregatedError` errors and
    # will format them accordingly, by showing all included errors in a single
    # response. See the `ApiErrors::AggregatedError` definition for more
    # information.
    class JsonApiFormatter < Formatter
      ##
      # Formats an `ApiErrors::ApiError` to a `Hash` object following the
      # JSON-API standard specification related to error objects.
      #
      # The following keys are accepted as error parameters:
      #
      # *   `tracker_link`: the error tracker's link can be specified, so that
      #     clients can have more information about the instance of the error.
      #     The `id` and `href` keys will be shown according to the JSON-API
      #     error specification.
      # *   `source`: the `pointer` and `parameter` keys will be shown in the
      #     output, as well as any references to the cause of the error.
      # *   `context`: this is a helper for `ActiveRecord` models. An instance
      #     of a model can be specified, and will be formatted as a `type` and
      #     `id` under `source`. A `type` and `id` keys can also be manually
      #     given, to prevent loading the record. Alternately, a single `type`
      #     key can be specified if the error is not related to a specific
      #     instance of a record.
      # *   `meta`: this hash will be outputted as is, and can contain anything
      #     that may help the client.
      #
      # An example follows:
      #
      # ```json
      # {
      #   "errors": [{
      #     "id": "34516",
      #     "links": {
      #       "about": "https://example.com/issues/7065/events/334516/"
      #     },
      #     "status": "404",
      #     "code": "not_found",
      #     "title": "User not found",
      #     "detail": "User with id 381 could not be found.",
      #     "source": {
      #       "type": "User",
      #       "id": 381,
      #       "pointer": "/data/attributes/user_id"
      #     },
      #     "meta": {
      #       "session_id": "8935259"
      #     }
      #   }]
      # }
      # ```
      def self.format_error(api_error)
        api_errors =
          case api_error
          when AggregatedError then api_error.api_errors
          else [api_error]
          end

        Errors.format(api_errors)
      end
    end
  end
end

require 'api_errors/formatters/json_api/error'
require 'api_errors/formatters/json_api/errors'
require 'api_errors/formatters/json_api/meta'
require 'api_errors/formatters/json_api/source'
require 'api_errors/formatters/json_api/tracker_link'
