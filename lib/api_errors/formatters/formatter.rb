module ApiErrors
  ##
  # The `ApiErrors::Formatters` module contains everything needed to convert an
  # `ApiErrors::ApiError` into a response to an API request.
  #
  # To create a new formatter, inherit from `ApiErrors::Formatters::Formatter`
  # and implement the `format_error` class method.
  module Formatters
    ##
    # Abstract formatter class.
    #
    # A formatter will format `ApiErrors::ApiError` instances, and produce some
    # output to be returned as responses to API requests.
    #
    # The following formatters are included in this gem:
    #
    # *   `ApiErrors::Formatters::JsonApiFormatter`: as per the JSON-API
    #     error specification.
    # *   `ApiErrors::Formatters::SimpleFormatter`: all attributes and
    #     parameters are returned.
    # *   `ApiErrors::Formatters::StringFormatter`: the code and detail
    #     attributes, as a string.
    class Formatter
      ##
      # Converts an API error to some arbitrary output, typically a `Hash`
      # object or a string.
      def self.format_error(_api_error)
        raise ::NotImplementedError
      end
    end
  end
end
