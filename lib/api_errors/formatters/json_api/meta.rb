module ApiErrors
  module Formatters
    class JsonApiFormatter
      # Helper class to format error metadata.
      class Meta # :nodoc:
        def self.format(params)
          meta = params[:meta]

          value = {}

          value[:meta] = meta if meta

          value
        end
      end
    end
  end
end
