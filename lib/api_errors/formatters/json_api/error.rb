module ApiErrors
  module Formatters
    class JsonApiFormatter
      # Helper class to format a single error.
      class Error # :nodoc:
        def self.format(api_error)
          params = api_error.params

          value = {
            status: api_error.status.to_s,
            code: api_error.code.to_s,
            title: api_error.title.to_s,
            detail: api_error.detail.to_s
          }

          value.merge!(TrackerLink.format(params))
          value.merge!(Source.format(params))
          value.merge!(Meta.format(params))
        end
      end
    end
  end
end
