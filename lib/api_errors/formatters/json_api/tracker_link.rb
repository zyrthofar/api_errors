module ApiErrors
  module Formatters
    class JsonApiFormatter
      # Helper class to format a link to an error tracker.
      class TrackerLink # :nodoc:
        def self.format(params)
          tracker_link = params[:tracker_link]

          value = {}

          if tracker_link
            id = tracker_link[:id]
            value[:id] = id.to_s if id

            href = tracker_link[:href]
            value[:links] = { about: href } if href
          end

          value
        end
      end
    end
  end
end
