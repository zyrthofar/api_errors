module ApiErrors
  module Formatters
    class JsonApiFormatter
      # Helper class to format the context of an error.
      class Source # :nodoc:
        def self.format(params)
          context = params[:context]
          source = params[:source]

          value = {}

          if context || source
            value[:source] = source || {}
            value[:source].merge!(format_context(context))
          end

          value
        end

        def self.format_context(context)
          value = {}

          if context.is_a?(Hash)
            value[:type] = context[:type]
            value[:id] = context[:id] if context[:id]
          elsif context
            value[:type] = context.class.name
            value[:id] = context.read_attribute(context.class.primary_key)
          end

          value
        end
      end
    end
  end
end
