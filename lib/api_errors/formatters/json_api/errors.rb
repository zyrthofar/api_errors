module ApiErrors
  module Formatters
    class JsonApiFormatter
      # Helper class to format a list of errors.
      class Errors # :nodoc:
        def self.format(api_errors)
          {
            errors: api_errors.collect { |e| Error.format(e) }
          }
        end
      end
    end
  end
end
