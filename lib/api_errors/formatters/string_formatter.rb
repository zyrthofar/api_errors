module ApiErrors
  module Formatters
    ##
    # `ApiErrors::Formatters::StringFormatter` formats an `ApiErrors::ApiError`
    # to a simple string.
    class StringFormatter < Formatter
      ##
      # Formats an `ApiErrors::ApiError` to a string containing the `code` and
      # `detail` attributes. Nothing else is included.
      #
      # An example follows:
      #
      # ```plain
      # "[not_found] User with id 381 could not be found."
      # ```
      def self.format_error(api_error)
        "[#{api_error.code}] #{api_error.detail}"
      end
    end
  end
end
