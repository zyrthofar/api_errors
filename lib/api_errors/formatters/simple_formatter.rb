module ApiErrors
  module Formatters
    ##
    # `ApiErrors::Formatters::SimpleFormatter` formats an `ApiErrors::ApiError`
    # to a `Hash` object.
    class SimpleFormatter < Formatter
      ##
      # Formats an `ApiErrors::ApiError` to a `Hash` object containing all of
      # the error attributes and parameters. Everything that is not an option
      # will be included in the result.
      #
      # An example follows:
      #
      # ```json
      # {
      #   "status": 404,
      #   "code": "not_found",
      #   "title": "User not found",
      #   "detail": "User with id 381 could not be found.",
      #   "level": "warning",
      #   "metadata": {
      #     "context_type": "User",
      #     "context_id": 381
      #   }
      # }
      # ```
      def self.format_error(api_error)
        {
          status: api_error.status,
          code: api_error.code,
          title: api_error.title,
          detail: api_error.detail
        }.merge!(api_error.params)
      end
    end
  end
end
