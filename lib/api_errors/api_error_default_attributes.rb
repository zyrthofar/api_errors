module ApiErrors
  ##
  # Contains the class- and instance-level methods to retrieve an error's
  # default attribute value.
  #
  # All defaults are stored as constants in their respective classes.
  module ApiErrorDefaultAttributes
    def self.included(base) # :nodoc:
      base.extend(ClassMethods)
    end

    ##
    # Retrieves the error's default status constant.
    def default_status
      self.class.default_status
    end

    ##
    # Retrieves the error's default code constant.
    def default_code
      self.class.default_code
    end

    ##
    # Retrieves the error's default title constant.
    def default_title
      self.class.default_title
    end

    ##
    # Retrieves the error's default detail constant.
    def default_detail
      self.class.default_detail
    end

    ##
    # Class-level methods.
    module ClassMethods # :nodoc:
      def default_status
        const_get(:DEFAULT_STATUS)
      end

      def default_code
        const_get(:DEFAULT_CODE)
      end

      def default_title
        const_get(:DEFAULT_TITLE)
      end

      def default_detail
        const_get(:DEFAULT_DETAIL)
      end
    end
  end
end
