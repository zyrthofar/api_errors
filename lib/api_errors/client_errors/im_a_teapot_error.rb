module ApiErrors
  # HTTP 418 - I'M A TEAPOT
  class ImATeapotError < ClientError # :nodoc:
    DEFAULT_STATUS = 418
    DEFAULT_CODE = 'im_a_teapot'.freeze
    DEFAULT_TITLE = "I'm A Teapot".freeze
    DEFAULT_DETAIL = "I'm a teapot error.".freeze
  end
end
