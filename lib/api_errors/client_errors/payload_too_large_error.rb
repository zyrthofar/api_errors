module ApiErrors
  # HTTP 413 - PAYLOAD TOO LARGE
  class PayloadTooLargeError < ClientError # :nodoc:
    DEFAULT_STATUS = 413
    DEFAULT_CODE = 'payload_too_large'.freeze
    DEFAULT_TITLE = 'Payload Too Large'.freeze
    DEFAULT_DETAIL = 'Payload too large error.'.freeze
  end
end
