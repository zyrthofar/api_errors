module ApiErrors
  # HTTP 422 - UNPROCESSABLE ENTITY
  class UnprocessableEntityError < ClientError # :nodoc:
    DEFAULT_STATUS = 422
    DEFAULT_CODE = 'unprocessable_entity'.freeze
    DEFAULT_TITLE = 'Unprocessable Entity'.freeze
    DEFAULT_DETAIL = 'Unprocessable entity error.'.freeze
  end
end
