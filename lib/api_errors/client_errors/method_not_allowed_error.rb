module ApiErrors
  # HTTP 405 - METHOD NOT ALLOWED
  class MethodNotAllowedError < ClientError # :nodoc:
    DEFAULT_STATUS = 405
    DEFAULT_CODE = 'method_not_allowed'.freeze
    DEFAULT_TITLE = 'Method Not Allowed'.freeze
    DEFAULT_DETAIL = 'Method not allowed error.'.freeze
  end
end
