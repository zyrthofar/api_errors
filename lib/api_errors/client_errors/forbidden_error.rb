module ApiErrors
  # HTTP 403 - FORBIDDEN
  class ForbiddenError < ClientError # :nodoc:
    DEFAULT_STATUS = 403
    DEFAULT_CODE = 'forbidden'.freeze
    DEFAULT_TITLE = 'Forbidden'.freeze
    DEFAULT_DETAIL = 'Forbidden error.'.freeze
  end
end
