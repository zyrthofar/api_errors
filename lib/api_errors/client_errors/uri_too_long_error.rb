module ApiErrors
  # HTTP 414 - URI TOO LONG
  class UriTooLongError < ClientError # :nodoc:
    DEFAULT_STATUS = 414
    DEFAULT_CODE = 'uri_too_long'.freeze
    DEFAULT_TITLE = 'URI Too Long'.freeze
    DEFAULT_DETAIL = 'URI too long error.'.freeze
  end
end
