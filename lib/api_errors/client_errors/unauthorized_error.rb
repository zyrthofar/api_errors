module ApiErrors
  # HTTP 401 - UNAUTHORIZED
  class UnauthorizedError < ClientError # :nodoc:
    DEFAULT_STATUS = 401
    DEFAULT_CODE = 'unauthorized'.freeze
    DEFAULT_TITLE = 'Unauthorized'.freeze
    DEFAULT_DETAIL = 'Unauthorized error.'.freeze
  end
end
