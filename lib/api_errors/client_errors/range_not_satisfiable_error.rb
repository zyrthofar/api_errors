module ApiErrors
  # HTTP 416 - RANGE NOT SATISFIABLE
  class RangeNotSatisfiableError < ClientError # :nodoc:
    DEFAULT_STATUS = 416
    DEFAULT_CODE = 'range_not_satisfiable'.freeze
    DEFAULT_TITLE = 'Range Not Satisfiable'.freeze
    DEFAULT_DETAIL = 'Range not satisfiable error.'.freeze
  end
end
