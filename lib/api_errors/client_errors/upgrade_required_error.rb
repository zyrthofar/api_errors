module ApiErrors
  # HTTP 426 - UPGRADE REQUIRED
  class UpgradeRequiredError < ClientError # :nodoc:
    DEFAULT_STATUS = 426
    DEFAULT_CODE = 'upgrade_required'.freeze
    DEFAULT_TITLE = 'Upgrade Required'.freeze
    DEFAULT_DETAIL = 'Upgrade required error.'.freeze
  end
end
