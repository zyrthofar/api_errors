module ApiErrors
  # HTTP 429 - REQUEST HEADER FIELDS TOO LARGE
  class RequestHeaderFieldsTooLargeError < ClientError # :nodoc:
    DEFAULT_STATUS = 431
    DEFAULT_CODE = 'request_header_fields_too_large'.freeze
    DEFAULT_TITLE = 'Request Header Fields Too Large'.freeze
    DEFAULT_DETAIL = 'Request header fields too large error.'.freeze
  end
end
