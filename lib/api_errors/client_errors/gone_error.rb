module ApiErrors
  # HTTP 410 - GONE
  class GoneError < ClientError # :nodoc:
    DEFAULT_STATUS = 410
    DEFAULT_CODE = 'gone'.freeze
    DEFAULT_TITLE = 'Gone'.freeze
    DEFAULT_DETAIL = 'Gone error.'.freeze
  end
end
