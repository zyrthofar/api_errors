module ApiErrors
  # HTTP 408 - REQUEST TIMEOUT
  class RequestTimeoutError < ClientError # :nodoc:
    DEFAULT_STATUS = 408
    DEFAULT_CODE = 'request_timeout'.freeze
    DEFAULT_TITLE = 'Request Timeout'.freeze
    DEFAULT_DETAIL = 'Request timeout error.'.freeze
  end
end
