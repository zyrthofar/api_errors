module ApiErrors
  # Base error for 4XX errors
  class ClientError < ApiError; end # :nodoc:
end

require 'api_errors/client_errors/bad_request_error'
require 'api_errors/client_errors/unauthorized_error'
require 'api_errors/client_errors/payment_required_error'
require 'api_errors/client_errors/forbidden_error'
require 'api_errors/client_errors/not_found_error'
require 'api_errors/client_errors/method_not_allowed_error'
require 'api_errors/client_errors/not_acceptable_error'
require 'api_errors/client_errors/proxy_authentication_required_error'
require 'api_errors/client_errors/request_timeout_error'
require 'api_errors/client_errors/conflict_error'
require 'api_errors/client_errors/gone_error'
require 'api_errors/client_errors/length_required_error'
require 'api_errors/client_errors/precondition_failed_error'
require 'api_errors/client_errors/payload_too_large_error'
require 'api_errors/client_errors/uri_too_long_error'
require 'api_errors/client_errors/unsupported_media_type_error'
require 'api_errors/client_errors/range_not_satisfiable_error'
require 'api_errors/client_errors/expectation_failed_error'
require 'api_errors/client_errors/im_a_teapot_error'
require 'api_errors/client_errors/misdirected_request_error'
require 'api_errors/client_errors/unprocessable_entity_error'
require 'api_errors/client_errors/locked_error'
require 'api_errors/client_errors/failed_dependency_error'
require 'api_errors/client_errors/upgrade_required_error'
require 'api_errors/client_errors/precondition_required_error'
require 'api_errors/client_errors/too_many_requests_error'
require 'api_errors/client_errors/request_header_fields_too_large_error'
require 'api_errors/client_errors/unavailable_for_legal_reasons_error'
