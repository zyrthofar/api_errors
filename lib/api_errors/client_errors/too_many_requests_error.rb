module ApiErrors
  # HTTP 429 - TOO MANY REQUESTS
  class TooManyRequestsError < ClientError # :nodoc:
    DEFAULT_STATUS = 429
    DEFAULT_CODE = 'too_many_requests'.freeze
    DEFAULT_TITLE = 'Too Many Requests'.freeze
    DEFAULT_DETAIL = 'Too many requests error.'.freeze
  end
end
