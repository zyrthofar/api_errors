module ApiErrors
  # HTTP 407 - PROXY AUTHENTICATION REQUIRED
  class ProxyAuthenticationRequiredError < ClientError # :nodoc:
    DEFAULT_STATUS = 407
    DEFAULT_CODE = 'proxy_authentication_required'.freeze
    DEFAULT_TITLE = 'Proxy Authentication Required'.freeze
    DEFAULT_DETAIL = 'Proxy authentication required error.'.freeze
  end
end
