module ApiErrors
  # HTTP 428 - PRECONDITION REQUIRED
  class PreconditionRequiredError < ClientError # :nodoc:
    DEFAULT_STATUS = 428
    DEFAULT_CODE = 'precondition_required'.freeze
    DEFAULT_TITLE = 'Precondition Required'.freeze
    DEFAULT_DETAIL = 'Precondition required error.'.freeze
  end
end
