module ApiErrors
  # HTTP 451 - UNAVAILABLE FOR LEGAL REASONS
  class UnavailableForLegalReasonsError < ClientError # :nodoc:
    DEFAULT_STATUS = 451
    DEFAULT_CODE = 'unavailable_for_legal_reasons'.freeze
    DEFAULT_TITLE = 'Unavailable For Legal Reasons'.freeze
    DEFAULT_DETAIL = 'Unavailable for legal reasons error.'.freeze
  end
end
