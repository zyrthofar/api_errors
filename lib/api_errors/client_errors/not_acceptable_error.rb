module ApiErrors
  # HTTP 406 - NOT ACCEPTABLE
  class NotAcceptableError < ClientError # :nodoc:
    DEFAULT_STATUS = 406
    DEFAULT_CODE = 'not_acceptable'.freeze
    DEFAULT_TITLE = 'Not Acceptable'.freeze
    DEFAULT_DETAIL = 'Not acceptable error.'.freeze
  end
end
