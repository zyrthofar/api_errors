module ApiErrors
  # HTTP 404 - NOT FOUND
  class NotFoundError < ClientError # :nodoc:
    DEFAULT_STATUS = 404
    DEFAULT_CODE = 'not_found'.freeze
    DEFAULT_TITLE = 'Not Found'.freeze
    DEFAULT_DETAIL = 'Not found error.'.freeze
  end
end
