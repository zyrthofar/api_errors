module ApiErrors
  # HTTP 411 - LENGTH REQUIRED
  class LengthRequiredError < ClientError # :nodoc:
    DEFAULT_STATUS = 411
    DEFAULT_CODE = 'length_required'.freeze
    DEFAULT_TITLE = 'Length Required'.freeze
    DEFAULT_DETAIL = 'Length required error.'.freeze
  end
end
