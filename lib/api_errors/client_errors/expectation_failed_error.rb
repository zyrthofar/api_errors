module ApiErrors
  # HTTP 417 - EXPECTATION FAILED
  class ExpectationFailedError < ClientError # :nodoc:
    DEFAULT_STATUS = 417
    DEFAULT_CODE = 'expectation_failed'.freeze
    DEFAULT_TITLE = 'Expectation Failed'.freeze
    DEFAULT_DETAIL = 'Expectation failed error.'.freeze
  end
end
