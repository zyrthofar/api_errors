module ApiErrors
  # HTTP 424 - FAILED DEPENDENCY
  class FailedDependencyError < ClientError # :nodoc:
    DEFAULT_STATUS = 424
    DEFAULT_CODE = 'failed_dependency'.freeze
    DEFAULT_TITLE = 'Failed Dependency'.freeze
    DEFAULT_DETAIL = 'Failed dependency error.'.freeze
  end
end
