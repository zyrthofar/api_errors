module ApiErrors
  # HTTP 415 - UNSUPPORTED MEDIA TYPE
  class UnsupportedMediaTypeError < ClientError # :nodoc:
    DEFAULT_STATUS = 415
    DEFAULT_CODE = 'unsupported_media_type'.freeze
    DEFAULT_TITLE = 'Unsupported Media Type'.freeze
    DEFAULT_DETAIL = 'Unsupported media type error.'.freeze
  end
end
