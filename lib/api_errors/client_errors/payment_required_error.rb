module ApiErrors
  # HTTP 402 - PAYMENT REQUIRED
  class PaymentRequiredError < ClientError # :nodoc:
    DEFAULT_STATUS = 402
    DEFAULT_CODE = 'payment_required'.freeze
    DEFAULT_TITLE = 'Payment Required'.freeze
    DEFAULT_DETAIL = 'Payment required error.'.freeze
  end
end
