module ApiErrors
  # HTTP 421 - MISDIRECTED REQUEST
  class MisdirectedRequestError < ClientError # :nodoc:
    DEFAULT_STATUS = 421
    DEFAULT_CODE = 'misdirected_request'.freeze
    DEFAULT_TITLE = 'Misdirected Request'.freeze
    DEFAULT_DETAIL = 'Misdirected request error.'.freeze
  end
end
