module ApiErrors
  # HTTP 423 - LOCKED
  class LockedError < ClientError # :nodoc:
    DEFAULT_STATUS = 423
    DEFAULT_CODE = 'locked'.freeze
    DEFAULT_TITLE = 'Locked'.freeze
    DEFAULT_DETAIL = 'Locked error.'.freeze
  end
end
