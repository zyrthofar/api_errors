module ApiErrors
  # HTTP 400 - BAD REQUEST
  class BadRequestError < ClientError # :nodoc:
    DEFAULT_STATUS = 400
    DEFAULT_CODE = 'bad_request'.freeze
    DEFAULT_TITLE = 'Bad Request'.freeze
    DEFAULT_DETAIL = 'Bad request error.'.freeze
  end
end
