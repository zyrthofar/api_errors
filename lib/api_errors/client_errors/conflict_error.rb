module ApiErrors
  # HTTP 409 - CONFLICT
  class ConflictError < ClientError # :nodoc:
    DEFAULT_STATUS = 409
    DEFAULT_CODE = 'conflict'.freeze
    DEFAULT_TITLE = 'Conflict'.freeze
    DEFAULT_DETAIL = 'Conflict error.'.freeze
  end
end
