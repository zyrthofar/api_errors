module ApiErrors
  # HTTP 412 - PRECONDITION FAILED
  class PreconditionFailedError < ClientError # :nodoc:
    DEFAULT_STATUS = 412
    DEFAULT_CODE = 'precondition_failed'.freeze
    DEFAULT_TITLE = 'Precondition Failed'.freeze
    DEFAULT_DETAIL = 'Precondition failed error.'.freeze
  end
end
