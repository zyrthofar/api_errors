module ApiErrors
  ##
  # Contains methods to translate API error attributes. Currently, only the
  # `title` and `detail` attributes can be translated.
  #
  # Translation is turned off by default, and can be configured globally or
  # individually by error.
  module ApiErrorTranslation
    ##
    # Returns the translation of the `title` attribute, or the attribute's
    # value if the translation is disabled.
    def title
      if translate_title?
        translate(:title, _title || default_title)
      else
        _title || default_title
      end
    end

    ##
    # Returns the translation of the `detail` attribute, or the attribute's
    # value if the translation is disabled.
    def detail
      if translate_detail?
        translate(:detail, _detail || default_detail)
      else
        _detail || default_detail
      end
    end

    private

    def translate(attribute, token)
      args_key = "translate_#{attribute}".to_sym
      args = options.fetch(args_key, {})
      args = {} if args == true

      # If 'token.attribute' is not found, try to find 'token'. If nothing is
      # found, use the token itself as the value.
      args[:default] = [token.to_sym, token.to_s]

      I18n.translate(
        "#{token}.#{attribute}",
        args
      )
    end

    def translate_title?
      options.fetch(:translate_title, translate?)
    end

    def translate_detail?
      options.fetch(:translate_detail, translate?)
    end

    def translate?
      options.fetch(:translate, ApiErrors.i18n_enabled?)
    end
  end
end
