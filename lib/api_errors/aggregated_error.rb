module ApiErrors
  ##
  # Aggregation of multiple API errors.
  #
  # `ApiErrors::AggregatedError`s mostly behave like a normal
  # `ApiErrors::ApiError`, with the exception of, in the hands of a compatible
  # formatter, being able to handle multiple errors at the same time.
  # The `ApiErrors::AggregatedError`s constructor can also receive a block,
  # which will aggregate all `ApiErrors::ApiError` inside it.
  #
  # Errors are added by using the `<<` method on the instance, and the `raise?`
  # method gives an indication of whether there are any errors in it.
  class AggregatedError < ApiError
    DEFAULT_STATUS = 400 # :nodoc:
    DEFAULT_CODE = 'multiple_api_errors'.freeze # :nodoc:
    DEFAULT_TITLE = 'Multiple API Errors'.freeze # :nodoc:
    DEFAULT_DETAIL = 'Multiple errors have occured.'.freeze # :nodoc:

    ##
    # Although typically unrequired, the aggregated errors are available
    # read-only, if anything more complicated needs to be done to the
    # aggregation.
    attr_reader :api_errors

    ##
    # Operator to add an error to the aggregation. The argument can either be an
    # instance of an `ApiErrors::ApiError`, or an `ApiErrors::ApiError` class.
    #
    #   aggregated_error << ApiErrors::BadRequestError.new
    #
    # Everytime an error is added, the `ApiErrors::AggregatedError` instance's
    # status is recalculated to keep it relevant. An idea of the calculation
    # follows:
    #
    # *   If all error statuses are the same, this status will be used for the
    #     aggregated status.
    # *   If all error statuses are `ApiErrors::ClientError`s or
    #     `ApiErrors::ServerError`s, the aggregated status will be a 400 or a
    #     500, respectively.
    # *   The aggregated status will otherwise be a 400.
    def <<(api_error)
      @api_errors += Array(api_error)
                       .collect { |e| ensure_instance(e) }
                       .compact

      @_status = compute_status

      self
    end

    ##
    # Method used to determine whether the `ApiErrors::AggregatedError` instance
    # should be raised.
    #
    #   raise aggregated_error if aggregated_error.raise?
    def raise?
      api_errors.count > 0
    end

    private

    ##
    # Creates an `ApiErrors::ApiError` to handle multiple errors at the same
    # time.
    #
    # A block can be given to the constructor, and the aggregated error will
    # be raised upon the block's completion if any errors were added.
    #
    # ```ruby
    # ApiErrors::AggregatedError.new do |agg|
    #   agg << ApiErrors::NotFoundError.new unless User.exists?(id: 381)
    # end
    # ```
    def initialize(*args, &block)
      @api_errors = []

      super

      if block_given?
        block.yield(self)
        raise self if raise?
      end
    end

    def ensure_instance(api_error)
      if api_error.is_a?(ApiError)
        api_error
      elsif api_error.is_a?(Class) && api_error <= ApiError
        api_error.new
      end
    end

    def compute_status
      return 400 if api_errors.none?

      if api_errors.collect(&:class).uniq.one?
        api_errors.first.status
      elsif api_errors.all? { |e| e.is_a?(ClientError) }
        400
      elsif api_errors.all? { |e| e.is_a?(ServerError) }
        500
      else
        400
      end
    end
  end
end
