module ApiErrors
  ##
  # The register is a list of `ApiErrors::ApiError` definitions that are keyed
  # by code. This code uniquely describes a type of error, and is used to
  # quickly instancize a new instance of this error type.
  #
  # It is recommended to use the `register_errors_from_yaml` configuration,
  # and the `ApiErrors.code` method, instead of manually calling the methods
  # in this class.
  class Register
    class CodeNotInRegistryError < RuntimeError; end # :nodoc:
    class InvalidClassError < RuntimeError; end # :nodoc:

    class << self
      ##
      # Adds a new error type to the registry, under the specified `code`.
      # The `args` argument must contain everything that are normally given
      # to the `ApiErrors::ApiError` constructor, although they will be merged
      # with the ones given at the time of instanciation with the
      # `build_error` method.
      def add(code, klass, *args)
        unless klass.is_a?(Class) && klass < ApiErrors::ApiError
          raise InvalidClassError,
                "Error class '#{klass}' is not an ApiError."
        end

        options = args.last.is_a?(Hash) ? args.pop.dup : {}

        @registry ||= {}
        @registry[code] = [klass, args, options]
      end

      ##
      # Creates a new instance of the error type specified by `code`. The
      # `detail` will override the one defined in the registry, and the
      # arguments will be merged with the basic arguments in the error
      # definition, so that runtime information can be added, for example an
      # object id.
      def build_error(code, detail, args)
        registry = @registry[code]

        if registry.nil?
          raise CodeNotInRegistryError,
                "Error code '#{code}' was not registered."
        end

        override_detail(registry[1], detail)

        registry[0].new(
          *registry[1],
          registry[2].dup.deep_merge!(args)
        )
      end

      private

      def override_detail(attributes, detail)
        if detail
          attributes.shift
          attributes.unshift(detail)
        end
      end
    end
  end
end
