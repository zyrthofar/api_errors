require 'api_errors/api_error_default_attributes'
require 'api_errors/api_error_translation'

module ApiErrors
  ##
  # `ApiErrors::ApiError`s are the basis of this gem. All official HTTP errors
  # map to a specific `ApiErrors::ApiError` class. Refer to the README.md file
  # for information about how to use these.
  class ApiError < StandardError
    ##
    # Default status
    DEFAULT_STATUS = 400

    ##
    # Default code
    DEFAULT_CODE = 'error'.freeze

    ##
    # Default title
    DEFAULT_TITLE = 'API Error'.freeze

    ##
    # Default detail
    DEFAULT_DETAIL = 'An error has occured.'.freeze

    # Class- and instance-level methods to retrieve an error's default attribute
    # value are in this module.
    include ApiErrorDefaultAttributes

    ##
    # Because they can be translated, the `title` and `detail` attribute
    # accessors are public methods that can be found in the
    # `ApiErrors::ApiErrorTranslation` module.
    include ApiErrorTranslation

    ##
    # The error's status, which should normally be a 4XX or 5XX HTTP status
    # code. The status will be returned as the response's HTTP status, and can
    # be included in the response's body if the formatter allows it. Each
    # `ApiErrors::ApiError` has their own default.
    def status
      _status || default_status
    end

    ##
    # The error's code. This attribute must be unique for a given error type.
    # This is the value that should be processed by the client, instead of the
    # `title` and `detail` attributes, which are human-readable strings that
    # serve only for displaying purposes. Each `ApiErrors::ApiError` has their
    # own default.
    def code
      _code || default_code
    end

    ##
    # The error parameters is a `Hash` object that can contain anything related
    # to the current error. Some formatters ignore some or all of these
    # parameters.
    def params
      _params || {}
    end

    ##
    # Processes the error, typically right before responding to the request.
    #
    # Nothing will be done if the error is set to be silent. Otherwise, the
    # error may log to the project's log file, and/or its information be sent
    # to an error tracking system.
    #
    # The `silent`, `log`, and `track` error parameters control the flow,
    # although the project must be set with instructions on what to do when
    # logging and tracking. See the `ApiErrors` module for more information.
    def process!
      return if silent?

      if log?
        callback = ApiErrors.log_callback
        callback.call(self) if callback.is_a?(Proc)
      end

      if track?
        callback = ApiErrors.track_callback
        callback.call(self) if callback.is_a?(Proc)
      end
    end

    ##
    # Converts the error to the requested format, to be used as the response's
    # body.
    def format
      formatter.format_error(self)
    end

    ##
    # Returns all error attributes and parameters in a `Hash` object. This is
    # used when processing an error, for example when sending the error to a
    # tracking system, so that all available information are sent, and not just
    # what the formatter uses.
    def as_json
      {
        status: status,
        code: code,
        title: title,
        detail: detail
      }.merge(params)
    end

    ##
    # Override of the `StandardError`'s `#to_s` method.
    def to_s
      "[#{code}] #{title} - #{detail}"
    end

    private

    attr_reader :_status,
                :_code,
                :_title,
                :_detail,
                :_params,
                :options

    ##
    # Creates a new instance of an `ApiErrors::ApiError`. The arguments must be
    # the error attributes - `detail`, `code`, `title`, and `status`, in this
    # order - followed by the error parameters and options, in a `Hash` object.
    #
    # Although it is recommended to specify them all, the attributes are
    # optional. You can set them `nil` when needed, and omit the latter ones.
    # The parameters and options are always the last argument.
    #
    # Following are some examples:
    #
    # ```ruby
    # ApiErrors::ApiError.new
    # ApiErrors::ApiError.new(detail)
    # ApiErrors::ApiError.new(nil, code, title, params_and_options)
    # ApiErrors::ApiError.new(params_and_options)
    # ```
    def initialize(*args)
      options = args.last.is_a?(Hash) ? args.pop.dup : {}

      @_detail = args.shift
      @_code = args.shift
      @_title = args.shift
      @_status = args.shift

      @_params = options.reject { |k| option_attributes.include?(k) }
      @options = options.select { |k| option_attributes.include?(k) }
    end

    def option_attributes
      %i[
        formatter
        translate
        translate_title
        translate_detail
        silent
        log
        track
      ]
    end

    def formatter
      formatter = options.fetch(:formatter, ApiErrors.formatter)
      formatter = formatter_from_symbol(formatter) if formatter.is_a?(Symbol)

      unless formatter && formatter < Formatters::Formatter
        raise ArgumentError, "Invalid formatter #{formatter}"
      end

      formatter
    end

    def formatter_from_symbol(symbol)
      case symbol
      when :json_api then Formatters::JsonApiFormatter
      when :simple then Formatters::SimpleFormatter
      when :string then Formatters::StringFormatter
      end
    end

    def silent?
      options.fetch(:silent, false)
    end

    def log?
      options.fetch(:log, !ApiErrors.log_callback.nil?)
    end

    def track?
      options.fetch(:track, !ApiErrors.track_callback.nil?)
    end
  end
end
