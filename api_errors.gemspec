Gem::Specification.new do |s|
  s.name = 'api_errors'
  s.version = '0.1.2'
  s.date = '2017-11-10'
  s.summary = 'API-related errors.'
  s.description = 'API-related errors and how to handle them.'
  s.authors = ['Zyrthofar']
  s.email = 'zyrthofar@gmail.com'
  s.homepage = 'https://gitlab.com/zyrthofar/api_errors'
  s.license = 'MIT'

  s.required_ruby_version = '>= 2.1.4'

  s.files = `git ls-files lib`.split($RS)
end
