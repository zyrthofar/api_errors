# README

`api_errors` is a ruby gem that can format errors to be returned by an API. Its
main goal is to unify all error responses and automate some underlying
processes.

## Basics

All official HTTP error codes in the 4XX and 5XX range are mapped to
`ApiErrors::ApiError` classes, for instance, `ApiErrors::NotFoundError`.

All of them have sensible attribute defaults that can, and normally should, be
overridden.

### Attributes

API errors have the following four basic attributes:

*   `status`: the HTTP code associated with the error.
*   `code`: a code associated to the error type. This value can be used by
    clients to process the error.
*   `title`: a short human-readable explanation of the error type. This value
    should never change between instances of the same error type, and is only
    for displaying purposes to the client. It should be localized if possible.
*   `detail`: a human-readable explanation of the instance of the error. Like
    `title`, it should only be used for displaying purposes, and should be
    localized if possible.

Not all formatters will use all attributes, but consider always specifying them
nonetheless.

### Parameters

API error parameters are all additional information given to the error for the
formatter, such as metadata. The parameters are a `Hash`, specified as the last
argument to the error. See individual formatters in the 'Formatters' section
for more information.

### Options

Some options can be set at the error level. The following are available:

*   `formatter`: which formatter to use when formatting the error. See the
    'Formatters' section for more information.
*   `translate` and `translate_*`: whether to translate the attributes using
    `I18n`. See the 'Internationalization' section for more information.
*   `log`: whether to log the error in the project's log file. Defaults to
    `true`.
*   `track`: whether to send the error to an error tracking service. Defaults
    to `true`.
*   `silent`: whether the error should be processed. Setting it to `true`
    effectively prevents the error from being logged or tracked. Defaults to
    `false`.

Options are passed as arguments to the error, like parameters, but they will be
removed and will never appear in the response.

## Usage

Basic error attributes can be specified to the initializer, in the following
order: `detail`, `code`, `title`, and `status`. All are optional, and the
latter ones can be omitted. `nil` attributes, and the omitted ones, will
receive the error's default values.

```ruby
ApiErrors::ApiError.new
ApiErrors::ApiError.new('detail', 'code')
ApiErrors::ApiError.new('detail', 'code', 'title', 400)
ApiErrors::ApiError.new(nil, nil, 'title')
```

Parameters and options are always specified as the last argument, regardless of
which attributes are given.

```ruby
ApiErrors::ApiError.new(params_and_options)
ApiErrors::ApiError.new('detail', 'code', params_and_options)
```

API errors can be raised normally, although `detail` only can be specified with
`raise`'s short form. If any other attributes, parameters, or options are
required, an instance of the error needs to the created and raised.

```ruby
raise ApiErrors::ApiError
raise ApiErrors::ApiError, 'detail'
raise ApiErrors::ApiError.new(...)
```

## Configuration

A `config/initializers/api_errors.rb` file can be created to specify initial
project configurations. An example is given below.

```ruby
ApiErrors.configure do |config|
  config.enable_i18n!
  config.track_callback = ->(api_error) { Raven.capture_exception(api_error) }
  config.register_errors_from_yaml('./config/api_errors.yml')
end
```

The following configurations are available:

*   `formatter=`: specifies the default formatter. Defaults to
    `Formatters::SimpleFormatter`. See the 'Formatters' section for more
    information about how formatters work.
*   `i18n_enabled!`: enables internationalization. Refer to the  the
    'Internationalization' section for more information.
*   `log_callback=`: specifies the procedure to call when logging the error.
*   `track_callback=`: specifies the procedure to call when tracking the error
    into an error tracking system.
*   `register_errors_from_yaml(file_path)`: specifies a yaml file to load error
    definitions from. See the 'Registry' section for more information.

## Error Processing

API errors can be logged to the project's log file, and tracked into an error
tracker. Both, if applicable, need to be specified in the project's initializer
file with a procedure.

```ruby
ApiErrors.configure do |config|
  config.log_callback = ->(api_error) { ... }
  config.track_callback = ->(api_error) { ... }
end
```

Each error instances can override whether to log and/or track, by using the
`log`, `track`, and `silent` boolean options.

Two methods are available for processing: `#format`, and `#as_json`.

### Format

When the error is returned as a response, the `#format` method converts the
error using the desired formatter.

Assuming a formatter that formats to a `Hash` object, the following Ruby on
Rails example catches all API errors and return the correct response:

```ruby
rescue_from ApiErrors::ApiError do |api_error|
  api_error.process!
  render json: api_error.format,
         status: api_error.status
end
```

### As JSON

The `#as_json` method will return all error attributes and parameters, so is
particularly useful when sending the error to an error tracking system.

The following error sends an API error to Sentry:

```ruby
ApiErrors.configure do |config|
  config.track_callback = ->(api_error) do
    Raven.capture_exception(api_error,
                            extra: { contents: api_error.as_json })
  end
end
```

## Formatters

The formatters define how the error will look like when returned as a response
to an API request.

By default, the `ApiErrors::Formatters::SimpleFormatter` will be used, but the
formatter can be specified in the project's initializer file.

```ruby
ApiErrors.configure do |config|
  config.formatter = :json_api
end
```

Any class inheriting from `ApiErrors::Formatters::Formatter` can be used. If
using a formatter defined in this gem, a symbol can be used instead.

### SimpleFormatter
This `ApiErrors::Formatters::SimpleFormatter`, or `:simple`, formatter outputs
a hash containing all attributes and parameters. An example follows, with
parameters `level` and `metadata`:

```json
{
  "status": 404,
  "code": "not_found",
  "title": "User not found",
  "detail": "User with id 381 could not be found.",
  "level": "warning",
  "metadata": {
    "context_type": "User",
    "context_id": 381
  }
}
```

### StringFormatter
The `ApiErrors::Formatters::StringFormatter`, or `:string`, formatter outputs a
string with the `code` and `detail` attributes, with the `code` enclosed in
brackets. It ignores the `title` and any parameters. An example follows:

```plain
'[not_found] User with id 381 could not be found.'
```

### JsonApiFormatter
The `ApiErrors::Formatters::JsonApiFormatter`, or `:json_api`, formatter
respects the JSON-API official error format. It uses all attributes, and the
following optional parameters:

*   `tracker_link`: the error tracker's link can be specified, so that clients
    can have more information about the instance of the error. The `id` and
    `href` keys will be shown according to the JSON-API error specification.
*   `source`: the `pointer` and `parameter` keys will be shown in the output,
    as well as any references to the cause of the error.
*   `context`: this is a helper for `ActiveRecord` models. An instance of a
    model can be specified, and will be formatted as a `type` and `id` under
    `source`. A `type` and `id` keys can also be manually given, to prevent
    loading the record. Alternately, a single `type` key can be specified if
    the error is not related to a specific instance of a record.
*   `meta`: this hash will be outputted as is, and can contain anything that
    may help the client.

An example follows:

```json
{
  "errors": [{
    "id": "34516",
    "links": {
      "about": "https://example.com/issues/7065/events/334516/"
    },
    "status": "404",
    "code": "not_found",
    "title": "User not found",
    "detail": "User with id 381 could not be found.",
    "source": {
      "type": "User",
      "id": 381,
      "pointer": "/data/attributes/user_id"
    },
    "meta": {
      "session_id": "8935259"
    }
  }]
}
```

ref. [JSON-API errors](http://jsonapi.org/format/#errors)

ref. [JSON-API error objects](http://jsonapi.org/examples/#error-objects)

#### Aggregated Errors

JSON-API's specification allows for multiple errors, which can be aggregated
into an `ApiErrors::AggregatedError` error. `ApiErrors::ApiError` errors can be
added to the aggregation using the `<<` operator.

The typical way of aggregating errors is to give a block to the constructor.
Errors can then be added to the `ApiErrors::AggregatedError` instance, and it
will be raised when the block ends if there were any errors added to it.

```ruby
ApiErrors::AggregatedError.new do |agg|
  agg << ApiErrors::NotFoundError.new if user.nil?
  agg << ApiErrors::ForbiddenError.new unless user.permitted?(:manage)
end
```

If you need to do this manually, the following has the same result:

```ruby
error = ApiErrors::AggregatedError.new

error << ApiErrors::NotFoundError.new if user.nil?
error << ApiErrors::ForbiddenError.new unless user.permitted?(:manage)

raise error if error.raise?
```

The `#raise?` method will return `true` if at least one error is in the
aggregation.

## Internationalization

Although turned off by default, `title` and `detail` attributes can be
translated. This can be set at the config, error, or attribute levels. An
example of each follows:

```ruby
# Config level, in the initializer file. All errors will be translated, unless
# the setting is overridden.
ApiErrors.configure do |config|
  config.enable_i18n!
end
```
```ruby
# Error level. Both `title` and `detail` attributes will be translated for this
# specific error only.
ApiErrors::ApiError.new(translate: true)
```
```ruby
# Attribute level. The `title` attribute will be translated, while the `detail`
# attribute will not.
ApiErrors::ApiError.new(translate_title: true)
```

### I18n keys

When an attribute is determined to be translatable, the attribute's value is
considered to be the translation key, to be used internally with
`I18n.translate()`. As a convenience, the key will first be tried with the
attribute name appended to it. Given a key of `'key'` on the `title` attribute,
the following will be tried until a value is found:

1. `I18n.translate('key.title')`
2. `I18n.translate('key')`
3. `'key'`

This means that, if your `title` translation is located at the key
`'key.error_title'`, the key `'key.error_title.title'` will first be tried and
fail. If possible, use the `'title'` and `'detail'` keys so you can benefit
from the short-form.

Default `title` and `detail` attributes are not translated.

### String Interpolation

If you have some values to be passed to `I18n.translate`, you need to set them
to the appropriate attribute's `translate_*` option. The example below will
interpolate any `%{id}` in the title's translation.

```ruby
ApiErrors::ApiError.new(translate_title: { id: 381 })
```

## Registry

A quick way to create instances of API errors and have all error definitions in
the same place, is to create one or more YAML files and define their attributes
there. These files can be loaded in the project's initializer file, as follows:

```ruby
ApiErrors.configure do |config|
  config.register_errors_from_yaml('./config/api_errors.yml')
end
```

The registry file is just a list of api error attributes, registered with a
code. Each kind of error should be registered under a unique code. A simple
example follows:

```yaml
a0001:
  error: ApiErrors::InternalServerError
  title: User Not Deleted
  detail: An error happened with the user, who could not be deleted...
  translate: false
a0002:
  error: ApiErrors::NotfoundError
  code: not_found
  title: errors.a0002
  detail: errors.a0002.details
  level: warning
  metadata:
    context_type: User
  translate: true
```

All attributes, parameters and options can be specified in the registry. If the
error code is not specified, it will become by default the registry code under
which the error kind is registered.

When the error occurs in the code, the error can be raised by using the
`ApiErrors.code` method, which will return a new instance of the error. The
error `detail` can be overridden if necessary, but by design, all other
attributes should never change between instances of an error type.

Additional parameters and options can be specified, for instance interpolation
values and metadata that are related to the specific instance of the error.

```ruby
raise ApiErrors.code(:a0001)

raise ApiErrors.code(:a0002,
                     translate_detail: { id: user.id },
                     metadata: {
                       context_id: user.id
                     })
```
