RSpec.describe ApiErrors do
  describe '.configure' do
    context 'when calling methods' do
      it 'executes these methods in the class' do
        expect(ApiErrors).to receive(:enable_i18n!)
        ApiErrors.configure(&:enable_i18n!)
      end
    end
  end

  describe '.formatter' do
    subject { ApiErrors.formatter }
    it { is_expected.to eq(ApiErrors::Formatters::SimpleFormatter) }
  end

  describe '.formatter=' do
    around(:example) do |example|
      formatter = ApiErrors.formatter
      example.run
      ApiErrors.formatter = formatter
    end

    it 'sets the default formatter' do
      ApiErrors.formatter = :string
      expect(ApiErrors.formatter).to eq(:string)
    end
  end

  describe '.i18n_enabled?' do
    subject { ApiErrors.i18n_enabled? }
    it { is_expected.to eq(false) }
  end

  describe '.enable_i18n!' do
    around(:example) do |example|
      translate = ApiErrors.i18n_enabled?
      example.run
      ApiErrors.instance_variable_set(:@i18n_enabled, translate)
    end

    it 'sets the translation value' do
      ApiErrors.enable_i18n!
      expect(ApiErrors.i18n_enabled?).to eq(true)
    end
  end

  describe '.log_callback=' do
    after(:example) { ApiErrors.log_callback = nil }
    let(:callback) { double(:callback) }

    it 'sets the log callback' do
      ApiErrors.log_callback = callback
      expect(ApiErrors.log_callback).to eq(callback)
    end
  end

  describe '.track_callback=' do
    after(:example) { ApiErrors.track_callback = nil }
    let(:callback) { double(:callback) }

    it 'sets the track callback' do
      ApiErrors.track_callback = callback
      expect(ApiErrors.track_callback).to eq(callback)
    end
  end

  describe '.register_errors_from_yaml' do
    let(:file_path) { './spec/support/registry_examples.yml' }
    subject { ApiErrors.register_errors_from_yaml(file_path) }

    it 'adds the errors to the registry' do
      expect(ApiErrors::Register).to(
        receive(:add)
        .with(
          :a0001,
          ApiErrors::ForbiddenError,
          'detail',
          'code',
          'title',
          499,
          {}
        )
      )

      expect(ApiErrors::Register).to(
        receive(:add)
        .with(
          :a0002,
          ApiErrors::BadRequestError,
          nil,
          'a0002',
          nil,
          nil,
          param1: 1,
          log: false
        )
      )

      subject
    end
  end

  describe '.register_error' do
    let(:code) { :a0003 }
    let(:args) do
      {
        status: 499,
        code: 'code',
        title: 'title',
        detail: 'detail',
        param1: 1
      }
    end

    subject { ApiErrors.register_error(code, args) }

    it 'adds the errors to the registry' do
      expect(ApiErrors::Register).to(
        receive(:add)
        .with(
          :a0003,
          ApiErrors::ApiError,
          'detail',
          'code',
          'title',
          499,
          param1: 1
        )
      )

      subject
    end

    context 'when the error key is specified' do
      let(:args) { { error: ApiErrors::UnauthorizedError.name } }
      it 'it correctly finds the error class' do
        expect(ApiErrors::Register).to(
          receive(:add)
          .with(
            :a0003,
            ApiErrors::UnauthorizedError,
            anything,
            anything,
            anything,
            anything,
            anything
          )
        )

        subject
      end
    end

    context 'when the status determines the error class' do
      let(:args) { { status: 405 } }
      it 'it correctly finds the error class' do
        expect(ApiErrors::Register).to(
          receive(:add)
          .with(
            :a0003,
            ApiErrors::MethodNotAllowedError,
            anything,
            anything,
            anything,
            anything,
            anything
          )
        )

        subject
      end
    end

    context 'when the code key is not specified' do
      let(:args) { {} }
      it 'uses the registry key by default' do
        expect(ApiErrors::Register).to(
          receive(:add)
          .with(
            :a0003,
            anything,
            anything,
            'a0003',
            anything,
            anything,
            anything
          )
        )

        subject
      end
    end
  end

  describe '.code' do
    let(:code) { 'code' }
    let(:detail) { 'detail' }
    let(:params) { { params: 1 } }
    let(:args) { [detail, params] }
    subject { ApiErrors.code(code, *args) }

    it 'defers to the registry' do
      expect(ApiErrors::Register)
        .to receive(:build_error).with('code', 'detail', params: 1)

      subject
    end

    context 'when no parameters are specified' do
      let(:args) { [detail] }

      it 'does not pass any parameters' do
        expect(ApiErrors::Register)
          .to receive(:build_error).with('code', 'detail', {})

        subject
      end
    end

    context 'when the detail is not specified' do
      let(:args) { [params] }

      it 'sets the detail to nil' do
        expect(ApiErrors::Register)
          .to receive(:build_error).with('code', nil, params)

        subject
      end
    end
  end
end
