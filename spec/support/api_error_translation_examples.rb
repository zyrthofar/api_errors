RSpec.shared_examples :translated_attribute do
  context 'when the attribute is not set to translate' do
    it { is_expected.to eq(value) }
  end

  context 'when the config is set to translate' do
    before(:example) do
      allow(ApiErrors).to receive(:i18n_enabled?).and_return(true)
    end
    it { is_expected.to eq(translated_value) }
  end

  context 'when the error is set to translate' do
    let(:args) { { translate: true } }
    it { is_expected.to eq(translated_value) }
  end

  context 'when the error attribute is set to translate' do
    let(:args) { { :"translate_#{attr}" => true } }
    it { is_expected.to eq(translated_value) }

    context 'and the short translation key is used' do
      let(:value) { 'tr' }
      it { is_expected.to eq(translated_value) }
    end

    context 'but the key could not be found' do
      let(:value) { 'tr.invalid' }
      it { is_expected.to eq('tr.invalid') }
    end
  end

  context 'when interpolation is needed' do
    let(:value) { 'tr.inter' }
    let(:args) { { :"translate_#{attr}" => { val: 'value' } } }
    it { is_expected.to eq('interpolation value') }
  end
end
