I18n.backend.store_translations(
  :en,
  tr: {
    title: 'title translation',
    detail: 'detail translation',
    inter: 'interpolation %<val>s'
  }
)
