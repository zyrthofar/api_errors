RSpec.shared_examples :api_error do
  subject { described_class.new(*args) }

  context 'when the detail is specified' do
    let(:args) { ['detail'] }
    it { is_expected.to have_attributes(detail: 'detail') }
  end

  context 'when the code is specified' do
    let(:args) { [nil, 'code'] }
    it { is_expected.to have_attributes(code: 'code') }
  end

  context 'when the title is specified' do
    let(:args) { [nil, nil, 'title'] }
    it { is_expected.to have_attributes(title: 'title') }
  end

  context 'when the status is specified' do
    let(:args) { [nil, nil, nil, 499] }
    it { is_expected.to have_attributes(status: 499) }
  end

  context 'when parameters are specified' do
    let(:args) { [{ param: 'param' }] }
    it { is_expected.to have_attributes(params: { param: 'param' }) }
  end

  context 'when the error is raised' do
    it 'raises the correct instance of the error' do
      begin
        error = described_class.new(
          'detail',
          'code',
          'title',
          param: 'param'
        )

        raise error
      rescue ApiErrors::ApiError => e
        expect(e).to be_a(described_class)
        expect(e).to have_attributes(detail: 'detail',
                                     title: 'title',
                                     code: 'code',
                                     params: { param: 'param' })
      end
    end

    context 'when the `raise` short form is used' do
      it 'raises the correct instance of the error' do
        begin
          raise described_class, 'detail'
        rescue ApiErrors::ApiError => e
          expect(e).to be_a(described_class)
          expect(e).to have_attributes(detail: 'detail')
        end
      end
    end
  end
end
