require 'support/translation_examples'
require 'support/api_error_translation_examples'

RSpec.describe ApiErrors::ApiError do
  let(:status) { 499 }
  let(:code) { 'code' }
  let(:title) { 'title' }
  let(:detail) { 'detail' }
  let(:args) { { arg1: 1 } }

  let(:error) { ApiErrors::ApiError.new(detail, code, title, status, args) }

  describe '#status' do
    subject { error.status }

    it { is_expected.to eq(499) }

    context 'when the status is not specified' do
      let(:status) { nil }
      it { is_expected.to eq(400) }
    end
  end

  describe '#code' do
    subject { error.code }

    it { is_expected.to eq('code') }

    context 'when the code is not specified' do
      let(:code) { nil }
      it { is_expected.to eq('error') }
    end
  end

  describe '#title' do
    subject { error.title }

    it { is_expected.to eq('title') }

    context 'when the title is not specified' do
      let(:title) { nil }
      it { is_expected.to eq('API Error') }
    end

    it_behaves_like :translated_attribute do
      let(:attr) { :title }
      let(:title) { value }
      let(:value) { 'tr.title' }
      let(:translated_value) { 'title translation' }
      let(:untranslated_value) { 'tr.title' }
    end
  end

  describe '#detail' do
    subject { error.detail }

    it { is_expected.to eq('detail') }

    context 'when the detail is not specified' do
      let(:detail) { nil }
      it { is_expected.to eq('An error has occured.') }
    end

    it_behaves_like :translated_attribute do
      let(:attr) { :detail }
      let(:detail) { value }
      let(:value) { 'tr.detail' }
      let(:translated_value) { 'detail translation' }
      let(:untranslated_value) { 'tr.detail' }
    end
  end

  describe '#params' do
    subject { error.params }

    it { is_expected.to match(arg1: 1) }

    context 'when options were passed' do
      let(:args) { { arg2: 2, log: true } }
      it { is_expected.to match(arg2: 2) }
    end
  end

  describe '#process!' do
    around(:example) do |example|
      ApiErrors.log_callback = log_callback
      ApiErrors.track_callback = track_callback
      example.run
      ApiErrors.log_callback = nil
      ApiErrors.track_callback = nil
    end

    let(:log_callback) { ->(api_error) {} }
    let(:track_callback) { proc { |api_error| } }
    let(:args) { {} }
    let(:api_error) { ApiErrors::ApiError.new(args) }
    subject { api_error.process! }

    it 'logs and tracks the error' do
      expect(log_callback).to receive(:call).with(api_error)
      expect(track_callback).to receive(:call).with(api_error)
      subject
    end

    context 'when the error is silent' do
      let(:args) { { silent: true } }
      it 'does not process the error' do
        expect(log_callback).not_to receive(:call)
        expect(track_callback).not_to receive(:call)
        subject
      end
    end

    context 'when the error is set not to log' do
      let(:args) { { log: false } }
      it 'does not process the error' do
        expect(log_callback).not_to receive(:call)
        allow(track_callback).to receive(:call)
        subject
      end
    end

    context 'when the error is set not to track' do
      let(:args) { { track: false } }
      it 'does not process the error' do
        allow(log_callback).to receive(:call)
        expect(track_callback).not_to receive(:call)
        subject
      end
    end
  end

  describe '#format' do
    subject { error.format }

    it 'formats the error using the default formatter' do
      expect(ApiErrors::Formatters::SimpleFormatter)
        .to receive(:format_error).with(error)

      subject
    end

    context 'when a default formatter is specified in the config' do
      it 'formats the error using the default formatter' do
        allow(ApiErrors).to receive(:formatter).and_return(:string)

        expect(ApiErrors::Formatters::StringFormatter)
          .to receive(:format_error).with(error)

        subject
      end
    end

    context 'when the formatter is specified in the options' do
      let(:args) { { formatter: ApiErrors::Formatters::StringFormatter } }
      it 'formats the error using the specified formatter' do
        expect(ApiErrors::Formatters::StringFormatter)
          .to receive(:format_error).with(error)

        subject
      end
    end

    context 'when the formatter is specified as a symbol' do
      let(:args) { { formatter: :string } }
      it 'formats the error using the specified formatter' do
        expect(ApiErrors::Formatters::StringFormatter)
          .to receive(:format_error).with(error)

        subject
      end
    end

    context 'when the formatter is invalid' do
      let(:args) { { formatter: :invalid } }
      it 'raises an argument error' do
        expect { subject }.to raise_error(ArgumentError)
      end
    end
  end

  describe '#as_json' do
    subject { error.as_json }

    it 'returns all attributes and parameters' do
      expect(subject).to eq(
        status: 499,
        code: 'code',
        title: 'title',
        detail: 'detail',
        arg1: 1
      )
    end
  end

  describe '#to_s' do
    subject { error.to_s }

    it { is_expected.to eq('[code] title - detail') }
  end
end
