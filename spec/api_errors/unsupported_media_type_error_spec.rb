require 'support/api_error_examples'

RSpec.describe ApiErrors::UnsupportedMediaTypeError do
  it_behaves_like :api_error

  context 'when no arguments are specified' do
    subject { ApiErrors::UnsupportedMediaTypeError.new }
    it 'has the default values' do
      expect(subject.status).to eq(415)
      expect(subject.code).to eq('unsupported_media_type')
      expect(subject.title).to eq('Unsupported Media Type')
      expect(subject.detail).to eq('Unsupported media type error.')
      expect(subject.params).to be_empty
    end
  end
end
