require 'support/api_error_examples'

RSpec.describe ApiErrors::BadRequestError do
  it_behaves_like :api_error

  context 'when no arguments are specified' do
    subject { ApiErrors::BadRequestError.new }
    it 'has the default values' do
      expect(subject.status).to eq(400)
      expect(subject.code).to eq('bad_request')
      expect(subject.title).to eq('Bad Request')
      expect(subject.detail).to eq('Bad request error.')
      expect(subject.params).to be_empty
    end
  end
end
