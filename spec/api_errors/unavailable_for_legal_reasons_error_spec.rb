require 'support/api_error_examples'

RSpec.describe ApiErrors::UnavailableForLegalReasonsError do
  it_behaves_like :api_error

  context 'when no arguments are specified' do
    subject { ApiErrors::UnavailableForLegalReasonsError.new }
    it 'has the default values' do
      expect(subject.status).to eq(451)
      expect(subject.code).to eq('unavailable_for_legal_reasons')
      expect(subject.title).to eq('Unavailable For Legal Reasons')
      expect(subject.detail).to eq('Unavailable for legal reasons error.')
      expect(subject.params).to be_empty
    end
  end
end
