require 'support/api_error_examples'

RSpec.describe ApiErrors::ServiceUnavailableError do
  it_behaves_like :api_error

  context 'when no arguments are specified' do
    subject { ApiErrors::ServiceUnavailableError.new }
    it 'has the default values' do
      expect(subject.status).to eq(503)
      expect(subject.code).to eq('service_unavailable')
      expect(subject.title).to eq('Service Unavailable')
      expect(subject.detail).to eq('Service unavailable error.')
      expect(subject.params).to be_empty
    end
  end
end
