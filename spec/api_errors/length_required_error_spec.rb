require 'support/api_error_examples'

RSpec.describe ApiErrors::LengthRequiredError do
  it_behaves_like :api_error

  context 'when no arguments are specified' do
    subject { ApiErrors::LengthRequiredError.new }
    it 'has the default values' do
      expect(subject.status).to eq(411)
      expect(subject.code).to eq('length_required')
      expect(subject.title).to eq('Length Required')
      expect(subject.detail).to eq('Length required error.')
      expect(subject.params).to be_empty
    end
  end
end
