require 'support/api_error_examples'

RSpec.describe ApiErrors::VariantAlsoNegotiatesError do
  it_behaves_like :api_error

  context 'when no arguments are specified' do
    subject { ApiErrors::VariantAlsoNegotiatesError.new }
    it 'has the default values' do
      expect(subject.status).to eq(506)
      expect(subject.code).to eq('variant_also_negotiates')
      expect(subject.title).to eq('Variant Also Negotiates')
      expect(subject.detail).to eq('Variant also negotiates error.')
      expect(subject.params).to be_empty
    end
  end
end
