require 'support/api_error_examples'

RSpec.describe ApiErrors::AggregatedError do
  it_behaves_like :api_error

  let(:instance) { ApiErrors::AggregatedError.new }

  describe '.new &block' do
    subject do
      ApiErrors::AggregatedError.new do |agg|
        agg << ApiErrors::ForbiddenError.new('error 1')
        agg << ApiErrors::NotFoundError.new('error 2')
      end
    end

    it 'raises the aggregated error' do
      expect { subject }.to raise_error do |agg|
        errors = agg.api_errors

        expect(errors.map(&:detail)).to eq(['error 1', 'error 2'])

        expect(errors.map(&:class)).to eq(
          [
            ApiErrors::ForbiddenError,
            ApiErrors::NotFoundError
          ]
        )
      end
    end

    context 'but no errors were present' do
      subject { ApiErrors::AggregatedError.new { |_agg| } }

      it 'does not raise the aggregated error' do
        expect { subject }.not_to raise_error
      end
    end
  end

  describe '#<<' do
    let(:api_error) { ApiErrors::NotFoundError.new }

    subject { instance << api_error }

    it 'adds the specified api error to the list' do
      subject
      expect(instance.api_errors.count).to eq(1)
      expect(instance.api_errors[0]).to be_a(ApiErrors::NotFoundError)
    end

    context 'when adding an api error class' do
      let(:api_error) { ApiErrors::NotFoundError }
      it 'adds an instance of the api error class to the list' do
        subject
        expect(instance.api_errors.count).to eq(1)
        expect(instance.api_errors[0]).to be_a(ApiErrors::NotFoundError)
      end
    end

    context 'when adding multiple api errors' do
      let(:api_error) do
        [
          ApiErrors::ForbiddenError.new,
          ApiErrors::BadRequestError.new
        ]
      end

      it 'adds all specified api errors to the list' do
        subject
        expect(instance.api_errors.count).to eq(2)
        expect(instance.api_errors[0]).to be_a(ApiErrors::ForbiddenError)
        expect(instance.api_errors[1]).to be_a(ApiErrors::BadRequestError)
      end
    end

    context 'when adding an invalid value' do
      let(:api_error) { :invalid }
      it 'does not add anything' do
        subject
        expect(instance.api_errors.count).to eq(0)
      end
    end

    context 'when computing the status' do
      subject do
        instance << api_errors
        instance.status
      end

      context 'but there are no api errors' do
        let(:api_errors) { [] }
        it { is_expected.to eq(400) }
      end

      context 'and all api errors are of the same type' do
        let(:api_errors) do
          [
            ApiErrors::NotFoundError.new,
            ApiErrors::NotFoundError.new
          ]
        end
        it { is_expected.to eq(404) }
      end

      context 'and all api errors are client errors' do
        let(:api_errors) do
          [
            ApiErrors::NotFoundError.new,
            ApiErrors::ForbiddenError.new
          ]
        end
        it { is_expected.to eq(400) }
      end

      context 'and all api errors are server errors' do
        let(:api_errors) do
          [
            ApiErrors::BadGatewayError.new,
            ApiErrors::GatewayTimeoutError.new
          ]
        end
        it { is_expected.to eq(500) }
      end

      context 'but all api errors are mixed' do
        let(:api_errors) do
          [
            ApiErrors::NotImplementedError.new,
            ApiErrors::LockedError.new
          ]
        end
        it { is_expected.to eq(400) }
      end
    end
  end

  describe '#raise?' do
    subject { instance.raise? }
    it { is_expected.to eq(false) }

    context 'when there are api errors in the list' do
      before(:example) { instance << ApiErrors::BadRequestError.new }
      it { is_expected.to eq(true) }
    end
  end

  context 'when no arguments are specified' do
    subject { ApiErrors::BadGatewayError.new }
    it 'has the default values' do
      expect(subject.status).to eq(502)
      expect(subject.code).to eq('bad_gateway')
      expect(subject.title).to eq('Bad Gateway')
      expect(subject.detail).to eq('Bad gateway error.')
      expect(subject.params).to be_empty
    end
  end
end
