require 'support/api_error_examples'

RSpec.describe ApiErrors::PreconditionFailedError do
  it_behaves_like :api_error

  context 'when no arguments are specified' do
    subject { ApiErrors::PreconditionFailedError.new }
    it 'has the default values' do
      expect(subject.status).to eq(412)
      expect(subject.code).to eq('precondition_failed')
      expect(subject.title).to eq('Precondition Failed')
      expect(subject.detail).to eq('Precondition failed error.')
      expect(subject.params).to be_empty
    end
  end
end
