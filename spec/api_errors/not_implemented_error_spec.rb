require 'support/api_error_examples'

RSpec.describe ApiErrors::NotImplementedError do
  it_behaves_like :api_error

  context 'when no arguments are specified' do
    subject { ApiErrors::NotImplementedError.new }
    it 'has the default values' do
      expect(subject.status).to eq(501)
      expect(subject.code).to eq('not_implemented')
      expect(subject.title).to eq('Not Implemented')
      expect(subject.detail).to eq('Not implemented error.')
      expect(subject.params).to be_empty
    end
  end
end
