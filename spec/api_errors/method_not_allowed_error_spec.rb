require 'support/api_error_examples'

RSpec.describe ApiErrors::MethodNotAllowedError do
  it_behaves_like :api_error

  context 'when no arguments are specified' do
    subject { ApiErrors::MethodNotAllowedError.new }
    it 'has the default values' do
      expect(subject.status).to eq(405)
      expect(subject.code).to eq('method_not_allowed')
      expect(subject.title).to eq('Method Not Allowed')
      expect(subject.detail).to eq('Method not allowed error.')
      expect(subject.params).to be_empty
    end
  end
end
