require 'support/api_error_examples'

RSpec.describe ApiErrors::FailedDependencyError do
  it_behaves_like :api_error

  context 'when no arguments are specified' do
    subject { ApiErrors::FailedDependencyError.new }
    it 'has the default values' do
      expect(subject.status).to eq(424)
      expect(subject.code).to eq('failed_dependency')
      expect(subject.title).to eq('Failed Dependency')
      expect(subject.detail).to eq('Failed dependency error.')
      expect(subject.params).to be_empty
    end
  end
end
