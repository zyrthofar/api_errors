require 'support/api_error_examples'

RSpec.describe ApiErrors::PaymentRequiredError do
  it_behaves_like :api_error

  context 'when no arguments are specified' do
    subject { ApiErrors::PaymentRequiredError.new }
    it 'has the default values' do
      expect(subject.status).to eq(402)
      expect(subject.code).to eq('payment_required')
      expect(subject.title).to eq('Payment Required')
      expect(subject.detail).to eq('Payment required error.')
      expect(subject.params).to be_empty
    end
  end
end
