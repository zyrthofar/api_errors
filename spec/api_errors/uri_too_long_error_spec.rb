require 'support/api_error_examples'

RSpec.describe ApiErrors::UriTooLongError do
  it_behaves_like :api_error

  context 'when no arguments are specified' do
    subject { ApiErrors::UriTooLongError.new }
    it 'has the default values' do
      expect(subject.status).to eq(414)
      expect(subject.code).to eq('uri_too_long')
      expect(subject.title).to eq('URI Too Long')
      expect(subject.detail).to eq('URI too long error.')
      expect(subject.params).to be_empty
    end
  end
end
