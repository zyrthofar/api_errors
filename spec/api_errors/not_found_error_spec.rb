require 'support/api_error_examples'

RSpec.describe ApiErrors::NotFoundError do
  it_behaves_like :api_error

  context 'when no arguments are specified' do
    subject { ApiErrors::NotFoundError.new }
    it 'has the default values' do
      expect(subject.status).to eq(404)
      expect(subject.code).to eq('not_found')
      expect(subject.title).to eq('Not Found')
      expect(subject.detail).to eq('Not found error.')
      expect(subject.params).to be_empty
    end
  end
end
