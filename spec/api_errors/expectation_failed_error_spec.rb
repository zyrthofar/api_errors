require 'support/api_error_examples'

RSpec.describe ApiErrors::ExpectationFailedError do
  it_behaves_like :api_error

  context 'when no arguments are specified' do
    subject { ApiErrors::ExpectationFailedError.new }
    it 'has the default values' do
      expect(subject.status).to eq(417)
      expect(subject.code).to eq('expectation_failed')
      expect(subject.title).to eq('Expectation Failed')
      expect(subject.detail).to eq('Expectation failed error.')
      expect(subject.params).to be_empty
    end
  end
end
