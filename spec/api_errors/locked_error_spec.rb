require 'support/api_error_examples'

RSpec.describe ApiErrors::LockedError do
  it_behaves_like :api_error

  context 'when no arguments are specified' do
    subject { ApiErrors::LockedError.new }
    it 'has the default values' do
      expect(subject.status).to eq(423)
      expect(subject.code).to eq('locked')
      expect(subject.title).to eq('Locked')
      expect(subject.detail).to eq('Locked error.')
      expect(subject.params).to be_empty
    end
  end
end
