RSpec.describe ApiErrors::Register do
  describe '.add' do
    let(:code) { :a0001 }
    let(:klass) { ApiErrors::UnauthorizedError }
    let(:args) do
      [
        'detail',
        'code',
        'title',
        {
          param1: 1,
          log: false
        }
      ]
    end

    subject { ApiErrors::Register.add(code, klass, *args) }

    it 'registers the error' do
      subject

      error = ApiErrors::Register.build_error(code, nil, {})
      expect(error).to be_a(ApiErrors::UnauthorizedError)
      expect(error.status).to eq(401)
      expect(error.code).to eq('code')
      expect(error.title).to eq('title')
      expect(error.detail).to eq('detail')
      expect(error.params[:param1]).to eq(1)
      expect(error.send(:log?)).to eq(false)
    end

    context 'when the error class is invalid' do
      let(:klass) { ApiErrors::BadRequestError.new }
      it 'raises an invalid class error' do
        expect { subject }
          .to raise_error(ApiErrors::Register::InvalidClassError)
      end
    end
  end

  describe '.build_error' do
    before(:example) do
      ApiErrors::Register.add(
        :a0001,
        ApiErrors::ForbiddenError,
        'detail',
        'code',
        'title',
        param1: 1,
        param3: { param4: 4 }
      )
    end

    let(:code) { :a0001 }
    let(:detail) { 'detail override' }
    let(:params) { { param2: 2, param3: { param5: 5 } } }
    let(:args) { [detail, params] }

    subject { ApiErrors::Register.build_error(code, *args) }

    it 'builds the error' do
      expect(subject).to be_a(ApiErrors::ForbiddenError)
      expect(subject.status).to eq(403)
      expect(subject.code).to eq('code')
      expect(subject.title).to eq('title')
      expect(subject.detail).to eq('detail override')
      expect(subject.params[:param1]).to eq(1)
      expect(subject.params[:param2]).to eq(2)
      expect(subject.params[:param3]).to eq(param4: 4, param5: 5)
    end

    context 'when the code is not in the registry' do
      let(:code) { :a0002 }
      it 'raises a code not in registry error' do
        expect { subject }
          .to raise_error(ApiErrors::Register::CodeNotInRegistryError)
      end
    end

    context 'when the detail is not specified' do
      let(:detail) { nil }

      it 'uses the detail form the registry' do
        expect(subject.detail).to eq('detail')
      end
    end
  end
end
