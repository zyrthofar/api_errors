require 'support/api_error_examples'

RSpec.describe ApiErrors::MisdirectedRequestError do
  it_behaves_like :api_error

  context 'when no arguments are specified' do
    subject { ApiErrors::MisdirectedRequestError.new }
    it 'has the default values' do
      expect(subject.status).to eq(421)
      expect(subject.code).to eq('misdirected_request')
      expect(subject.title).to eq('Misdirected Request')
      expect(subject.detail).to eq('Misdirected request error.')
      expect(subject.params).to be_empty
    end
  end
end
