require 'support/api_error_examples'

RSpec.describe ApiErrors::PreconditionRequiredError do
  it_behaves_like :api_error

  context 'when no arguments are specified' do
    subject { ApiErrors::PreconditionRequiredError.new }
    it 'has the default values' do
      expect(subject.status).to eq(428)
      expect(subject.code).to eq('precondition_required')
      expect(subject.title).to eq('Precondition Required')
      expect(subject.detail).to eq('Precondition required error.')
      expect(subject.params).to be_empty
    end
  end
end
