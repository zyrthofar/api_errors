require 'support/api_error_examples'

RSpec.describe ApiErrors::RequestTimeoutError do
  it_behaves_like :api_error

  context 'when no arguments are specified' do
    subject { ApiErrors::RequestTimeoutError.new }
    it 'has the default values' do
      expect(subject.status).to eq(408)
      expect(subject.code).to eq('request_timeout')
      expect(subject.title).to eq('Request Timeout')
      expect(subject.detail).to eq('Request timeout error.')
      expect(subject.params).to be_empty
    end
  end
end
