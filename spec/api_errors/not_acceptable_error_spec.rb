require 'support/api_error_examples'

RSpec.describe ApiErrors::NotAcceptableError do
  it_behaves_like :api_error

  context 'when no arguments are specified' do
    subject { ApiErrors::NotAcceptableError.new }
    it 'has the default values' do
      expect(subject.status).to eq(406)
      expect(subject.code).to eq('not_acceptable')
      expect(subject.title).to eq('Not Acceptable')
      expect(subject.detail).to eq('Not acceptable error.')
      expect(subject.params).to be_empty
    end
  end
end
