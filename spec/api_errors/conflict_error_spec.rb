require 'support/api_error_examples'

RSpec.describe ApiErrors::ConflictError do
  it_behaves_like :api_error

  context 'when no arguments are specified' do
    subject { ApiErrors::ConflictError.new }
    it 'has the default values' do
      expect(subject.status).to eq(409)
      expect(subject.code).to eq('conflict')
      expect(subject.title).to eq('Conflict')
      expect(subject.detail).to eq('Conflict error.')
      expect(subject.params).to be_empty
    end
  end
end
