RSpec.describe ApiErrors::Formatters::Formatter do
  let(:error) { ApiErrors::ApiError.new }

  describe '.format_error' do
    subject { ApiErrors::Formatters::Formatter.format_error(error) }

    it 'raises a NotImplementedError' do
      expect { subject }.to raise_error ::NotImplementedError
    end
  end
end
