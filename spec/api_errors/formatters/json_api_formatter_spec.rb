RSpec.describe ApiErrors::Formatters::JsonApiFormatter do
  let(:status) { 499 }
  let(:code) { 'code' }
  let(:title) { 'title' }
  let(:detail) { 'detail' }
  let(:args) { {} }

  let(:error) { ApiErrors::ApiError.new(detail, code, title, status, args) }

  describe '.format_error' do
    subject { ApiErrors::Formatters::JsonApiFormatter.format_error(error) }

    it 'contains the expected attributes' do
      error = subject[:errors].first

      expect(error).to match(
        status: '499',
        code: 'code',
        title: 'title',
        detail: 'detail'
      )
    end

    context 'when a tracker link is specified' do
      let(:args) { { tracker_link: tracker_link } }
      let(:tracker_link) { { id: 1, href: 'url' } }

      it 'contains the tracker link' do
        error = subject[:errors].first

        expect(error[:id]).to eq('1')
        expect(error[:links]).to match(about: 'url')
      end
    end

    context 'when a source is specified' do
      let(:args) { { source: source } }
      let(:source) { { pointer: 'pointer', other: 'value' } }

      it 'contains the source' do
        error = subject[:errors].first

        expect(error[:source][:pointer]).to eq('pointer')
        expect(error[:source][:other]).to eq('value')
      end
    end

    context 'when a context record is specified' do
      before(:example) do
        # This gem doesn't need ActiveRecord. Simulate one instead.
        allow(record).to receive(:class).and_return(record_class)
        allow(record).to receive(:read_attribute).with('id').and_return(1)
        allow(record_class).to receive(:name).and_return('name')
        allow(record_class).to receive(:primary_key).and_return('id')
      end

      let(:args) { { context: context } }
      let(:context) { record }
      let(:record) { double(:record) }
      let(:record_class) { double(:record_class) }

      it 'contains the context type and id' do
        error = subject[:errors].first

        expect(error[:source][:type]).to eq('name')
        expect(error[:source][:id]).to eq(1)
      end
    end

    context 'when a context type is specified' do
      let(:args) { { context: context } }
      let(:context) { { type: 'type' } }

      it 'contains the context type' do
        error = subject[:errors].first

        expect(error[:source][:type]).to eq('type')
        expect(error[:source]).not_to have_key(:id)
      end
    end

    context 'when a context type and id are specified' do
      let(:args) { { context: context } }
      let(:context) { { type: 'type', id: 'id' } }

      it 'contains the context type and id' do
        error = subject[:errors].first

        expect(error[:source][:type]).to eq('type')
        expect(error[:source][:id]).to eq('id')
      end
    end

    context 'when metadata is specified' do
      let(:args) { { meta: metadata } }
      let(:metadata) { { meta1: 1 } }

      it 'contains the metadata' do
        error = subject[:errors].first

        expect(error[:meta]).to match(meta1: 1)
      end
    end

    context 'when an aggregated error is formatted' do
      before(:example) do
        error << ApiErrors::NotFoundError.new
        error << ApiErrors::ForbiddenError.new
      end

      let(:error) { ApiErrors::AggregatedError.new }

      it 'shows all aggregated errors' do
        errors = subject[:errors]

        expect(errors.count).to eq(2)
        expect(errors[0][:status]).to eq('404')
        expect(errors[1][:status]).to eq('403')
      end
    end
  end

  describe 'symbol representation' do
    it 'uses the JsonApiFormatter' do
      expect(ApiErrors::Formatters::JsonApiFormatter).to receive(:format_error)
      ApiErrors::ApiError.new(formatter: :json_api).format
    end
  end
end
