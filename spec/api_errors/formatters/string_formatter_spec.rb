RSpec.describe ApiErrors::Formatters::StringFormatter do
  let(:status) { 499 }
  let(:code) { 'code' }
  let(:title) { 'title' }
  let(:detail) { 'detail' }
  let(:args) { {} }

  let(:error) { ApiErrors::ApiError.new(detail, code, title, status, args) }

  describe '.format_error' do
    subject { ApiErrors::Formatters::StringFormatter.format_error(error) }

    it { is_expected.to eq('[code] detail') }
  end

  describe 'symbol representation' do
    it 'uses the StringFormatter' do
      expect(ApiErrors::Formatters::StringFormatter).to receive(:format_error)
      ApiErrors::ApiError.new(formatter: :string).format
    end
  end
end
