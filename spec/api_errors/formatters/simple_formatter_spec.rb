RSpec.describe ApiErrors::Formatters::SimpleFormatter do
  let(:status) { 499 }
  let(:code) { 'code' }
  let(:title) { 'title' }
  let(:detail) { 'detail' }
  let(:args) { {} }

  let(:error) { ApiErrors::ApiError.new(detail, code, title, status, args) }

  describe '.format_error' do
    subject { ApiErrors::Formatters::SimpleFormatter.format_error(error) }

    it 'has all error attributes' do
      expect(subject).to eq(
        status: 499,
        code: 'code',
        title: 'title',
        detail: 'detail'
      )
    end

    context 'when parameters are specified' do
      let(:args) { { id: '123', meta: { value: 'metadata' } } }

      it 'contains the parameters' do
        expect(subject[:id]).to eq('123')
        expect(subject[:meta]).to eq(value: 'metadata')
      end

      context 'but options are also specified' do
        let(:args) { { log: false } }
        it { is_expected.not_to have_key(:log) }
      end
    end
  end

  describe 'symbol representation' do
    it 'uses the SimpleFormatter' do
      expect(ApiErrors::Formatters::SimpleFormatter).to receive(:format_error)
      ApiErrors::ApiError.new(formatter: :simple).format
    end
  end
end
