require 'support/api_error_examples'

RSpec.describe ApiErrors::UpgradeRequiredError do
  it_behaves_like :api_error

  context 'when no arguments are specified' do
    subject { ApiErrors::UpgradeRequiredError.new }
    it 'has the default values' do
      expect(subject.status).to eq(426)
      expect(subject.code).to eq('upgrade_required')
      expect(subject.title).to eq('Upgrade Required')
      expect(subject.detail).to eq('Upgrade required error.')
      expect(subject.params).to be_empty
    end
  end
end
