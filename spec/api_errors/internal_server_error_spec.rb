require 'support/api_error_examples'

RSpec.describe ApiErrors::InternalServerError do
  it_behaves_like :api_error

  context 'when no arguments are specified' do
    subject { ApiErrors::InternalServerError.new }
    it 'has the default values' do
      expect(subject.status).to eq(500)
      expect(subject.code).to eq('internal_server_error')
      expect(subject.title).to eq('Internal Server Error')
      expect(subject.detail).to eq('Internal server error.')
      expect(subject.params).to be_empty
    end
  end
end
