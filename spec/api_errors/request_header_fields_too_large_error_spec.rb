require 'support/api_error_examples'

RSpec.describe ApiErrors::RequestHeaderFieldsTooLargeError do
  it_behaves_like :api_error

  context 'when no arguments are specified' do
    subject { ApiErrors::RequestHeaderFieldsTooLargeError.new }
    it 'has the default values' do
      expect(subject.status).to eq(431)
      expect(subject.code).to eq('request_header_fields_too_large')
      expect(subject.title).to eq('Request Header Fields Too Large')
      expect(subject.detail).to eq('Request header fields too large error.')
      expect(subject.params).to be_empty
    end
  end
end
