require 'support/api_error_examples'

RSpec.describe ApiErrors::UnprocessableEntityError do
  it_behaves_like :api_error

  context 'when no arguments are specified' do
    subject { ApiErrors::UnprocessableEntityError.new }
    it 'has the default values' do
      expect(subject.status).to eq(422)
      expect(subject.code).to eq('unprocessable_entity')
      expect(subject.title).to eq('Unprocessable Entity')
      expect(subject.detail).to eq('Unprocessable entity error.')
      expect(subject.params).to be_empty
    end
  end
end
