require 'support/api_error_examples'

RSpec.describe ApiErrors::UnauthorizedError do
  it_behaves_like :api_error

  context 'when no arguments are specified' do
    subject { ApiErrors::UnauthorizedError.new }
    it 'has the default values' do
      expect(subject.status).to eq(401)
      expect(subject.code).to eq('unauthorized')
      expect(subject.title).to eq('Unauthorized')
      expect(subject.detail).to eq('Unauthorized error.')
      expect(subject.params).to be_empty
    end
  end
end
