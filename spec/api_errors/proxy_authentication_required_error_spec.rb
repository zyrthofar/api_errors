require 'support/api_error_examples'

RSpec.describe ApiErrors::ProxyAuthenticationRequiredError do
  it_behaves_like :api_error

  context 'when no arguments are specified' do
    subject { ApiErrors::ProxyAuthenticationRequiredError.new }
    it 'has the default values' do
      expect(subject.status).to eq(407)
      expect(subject.code).to eq('proxy_authentication_required')
      expect(subject.title).to eq('Proxy Authentication Required')
      expect(subject.detail).to eq('Proxy authentication required error.')
      expect(subject.params).to be_empty
    end
  end
end
