require 'support/api_error_examples'

RSpec.describe ApiErrors::PayloadTooLargeError do
  it_behaves_like :api_error

  context 'when no arguments are specified' do
    subject { ApiErrors::PayloadTooLargeError.new }
    it 'has the default values' do
      expect(subject.status).to eq(413)
      expect(subject.code).to eq('payload_too_large')
      expect(subject.title).to eq('Payload Too Large')
      expect(subject.detail).to eq('Payload too large error.')
      expect(subject.params).to be_empty
    end
  end
end
