require 'support/api_error_examples'

RSpec.describe ApiErrors::NotExtendedError do
  it_behaves_like :api_error

  context 'when no arguments are specified' do
    subject { ApiErrors::NotExtendedError.new }
    it 'has the default values' do
      expect(subject.status).to eq(510)
      expect(subject.code).to eq('not_extended')
      expect(subject.title).to eq('Not Extended')
      expect(subject.detail).to eq('Not extended error.')
      expect(subject.params).to be_empty
    end
  end
end
