require 'support/api_error_examples'

RSpec.describe ApiErrors::HttpVersionNotSupportedError do
  it_behaves_like :api_error

  context 'when no arguments are specified' do
    subject { ApiErrors::HttpVersionNotSupportedError.new }
    it 'has the default values' do
      expect(subject.status).to eq(505)
      expect(subject.code).to eq('http_version_not_supported')
      expect(subject.title).to eq('HTTP Version Not Supported')
      expect(subject.detail).to eq('HTTP version not supported error.')
      expect(subject.params).to be_empty
    end
  end
end
