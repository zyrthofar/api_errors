require 'support/api_error_examples'

RSpec.describe ApiErrors::NetworkAuthenticationRequiredError do
  it_behaves_like :api_error

  context 'when no arguments are specified' do
    subject { ApiErrors::NetworkAuthenticationRequiredError.new }
    it 'has the default values' do
      expect(subject.status).to eq(511)
      expect(subject.code).to eq('network_authentication_required')
      expect(subject.title).to eq('Network Authentication Required')
      expect(subject.detail).to eq('Network authentication required error.')
      expect(subject.params).to be_empty
    end
  end
end
