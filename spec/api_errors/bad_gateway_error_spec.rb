require 'support/api_error_examples'

RSpec.describe ApiErrors::BadGatewayError do
  it_behaves_like :api_error

  context 'when no arguments are specified' do
    subject { ApiErrors::BadGatewayError.new }
    it 'has the default values' do
      expect(subject.status).to eq(502)
      expect(subject.code).to eq('bad_gateway')
      expect(subject.title).to eq('Bad Gateway')
      expect(subject.detail).to eq('Bad gateway error.')
      expect(subject.params).to be_empty
    end
  end
end
