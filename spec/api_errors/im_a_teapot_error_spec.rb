require 'support/api_error_examples'

RSpec.describe ApiErrors::ImATeapotError do
  it_behaves_like :api_error

  context 'when no arguments are specified' do
    subject { ApiErrors::ImATeapotError.new }
    it 'has the default values' do
      expect(subject.status).to eq(418)
      expect(subject.code).to eq('im_a_teapot')
      expect(subject.title).to eq("I'm A Teapot")
      expect(subject.detail).to eq("I'm a teapot error.")
      expect(subject.params).to be_empty
    end
  end
end
