require 'support/api_error_examples'

RSpec.describe ApiErrors::ForbiddenError do
  it_behaves_like :api_error

  context 'when no arguments are specified' do
    subject { ApiErrors::ForbiddenError.new }
    it 'has the default values' do
      expect(subject.status).to eq(403)
      expect(subject.code).to eq('forbidden')
      expect(subject.title).to eq('Forbidden')
      expect(subject.detail).to eq('Forbidden error.')
      expect(subject.params).to be_empty
    end
  end
end
