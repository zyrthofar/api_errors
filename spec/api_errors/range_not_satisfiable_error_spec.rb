require 'support/api_error_examples'

RSpec.describe ApiErrors::RangeNotSatisfiableError do
  it_behaves_like :api_error

  context 'when no arguments are specified' do
    subject { ApiErrors::RangeNotSatisfiableError.new }
    it 'has the default values' do
      expect(subject.status).to eq(416)
      expect(subject.code).to eq('range_not_satisfiable')
      expect(subject.title).to eq('Range Not Satisfiable')
      expect(subject.detail).to eq('Range not satisfiable error.')
      expect(subject.params).to be_empty
    end
  end
end
