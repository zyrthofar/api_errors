require 'support/api_error_examples'

RSpec.describe ApiErrors::LoopDetectedError do
  it_behaves_like :api_error

  context 'when no arguments are specified' do
    subject { ApiErrors::LoopDetectedError.new }
    it 'has the default values' do
      expect(subject.status).to eq(508)
      expect(subject.code).to eq('loop_detected')
      expect(subject.title).to eq('Loop Detected')
      expect(subject.detail).to eq('Loop detected error.')
      expect(subject.params).to be_empty
    end
  end
end
