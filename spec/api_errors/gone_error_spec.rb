require 'support/api_error_examples'

RSpec.describe ApiErrors::GoneError do
  it_behaves_like :api_error

  context 'when no arguments are specified' do
    subject { ApiErrors::GoneError.new }
    it 'has the default values' do
      expect(subject.status).to eq(410)
      expect(subject.code).to eq('gone')
      expect(subject.title).to eq('Gone')
      expect(subject.detail).to eq('Gone error.')
      expect(subject.params).to be_empty
    end
  end
end
