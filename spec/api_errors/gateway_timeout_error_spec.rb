require 'support/api_error_examples'

RSpec.describe ApiErrors::GatewayTimeoutError do
  it_behaves_like :api_error

  context 'when no arguments are specified' do
    subject { ApiErrors::GatewayTimeoutError.new }
    it 'has the default values' do
      expect(subject.status).to eq(504)
      expect(subject.code).to eq('gateway_timeout')
      expect(subject.title).to eq('Gateway Timeout')
      expect(subject.detail).to eq('Gateway timeout error.')
      expect(subject.params).to be_empty
    end
  end
end
