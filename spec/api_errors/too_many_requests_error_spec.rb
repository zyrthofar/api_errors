require 'support/api_error_examples'

RSpec.describe ApiErrors::TooManyRequestsError do
  it_behaves_like :api_error

  context 'when no arguments are specified' do
    subject { ApiErrors::TooManyRequestsError.new }
    it 'has the default values' do
      expect(subject.status).to eq(429)
      expect(subject.code).to eq('too_many_requests')
      expect(subject.title).to eq('Too Many Requests')
      expect(subject.detail).to eq('Too many requests error.')
      expect(subject.params).to be_empty
    end
  end
end
