require 'support/api_error_examples'

RSpec.describe ApiErrors::InsufficientStorageError do
  it_behaves_like :api_error

  context 'when no arguments are specified' do
    subject { ApiErrors::InsufficientStorageError.new }
    it 'has the default values' do
      expect(subject.status).to eq(507)
      expect(subject.code).to eq('insufficient_storage')
      expect(subject.title).to eq('Insufficient Storage')
      expect(subject.detail).to eq('Insufficient storage error.')
      expect(subject.params).to be_empty
    end
  end
end
